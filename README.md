# opensuse-qt5-devel

A Opensuse Tumbleweed based image with packages for Qt5 development included.


# Instructions

```
$git clone git@gitlab.com:cutecom/opensuse-qt5-devel.git
$cd opensuse-qt5-devel.git
# make changes

#inspired by https://container-solutions.com/tagging-docker-images-the-right-way/

$git log -1 

$docker build -t
cutecom/opensuse-qt5-devel:<GIT-HEAD-SHA1-FROM-PREVIOUS-COMMNAD> -t cutecom/opensuse-qt5-devel:latest .

$docker login
Login with your Docker ID to push and pull

$docker images

$docker push cutecom/opensuse-qt5-devel
```

Head over to https://hub.docker.com/r/cutecom/opensuse-qt5-devel/ to verify result
