FROM	opensuse/tumbleweed
LABEL	maintainer="Meinhard Ritscher <cyc1ingsir@gmail.com>" \
 version="0.5" \
 git-url="https://gitlab.com/cutecom/opensuse-qt5-build"


RUN zypper ref &&\
    zypper --non-interactive in -t pattern devel_qt5 &&\
    zypper --non-interactive in\
    libQt5Widgets-devel \
    libQt5Network-devel \
    libqt5-qtserialport-devel \
    git-core \
    python \
    clang6
